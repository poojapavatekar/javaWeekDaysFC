package constructors;

public class Car {
	
	// properties - variables
	String model;
	String mfr;
	int wheels;
	double price;
	String color;
	
	
	
//	Car(){
//		System.out.println("In an empty constructor");
//	}
	
	Car(String model,String mfr,int wheels, double price,String color ){
		this.model=model;
		this.mfr=mfr;
		this.price=price;
		this.wheels=wheels;
		this.color=color;
		
	}
	
	
	
	// behaviours - functions 
	
	public void start(){
		System.out.println("In CAR start function");
		// generate a random number
	}
	
	public void stop(){
		System.out.println("In CAR stop function");

	}
	
	
}
