package inheritance;

public class ex {

	
	
	public void xyz(){
		System.out.println("In xyz");
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		ex x=new ex();
		x.xyz();
		
		Car c= new Car();
		c.color="Red";
			
		c.mfr="Honda";
		c.model="City";
		c.price=88888.67;
		c.wheels=6;
		
		
		c.start();
		c.stop();
		System.out.println("****************************");
		
		SUV s= new SUV();
		s.mfr="Mahindra";
		s.model="Xylo";
		s.price=9999999999.9;
		s.wheels=4;
		s.speed=90;
		s.start();
		s.move();
		s.stop();
	}

}
