package Basics;

public class ops {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		int i=9;
		int j=6;
		
		// arithmatic  ----    +  -  *  /  %    ---- returns u an integer value
		System.out.println("i+j : "+ (i+j));
		System.out.println("i-j : "+ (i-j));
		System.out.println("i*j : "+ (i*j));
		System.out.println("i/j : "+ (i/j));
		System.out.println("i%j : "+ (i%j));
		
		
		// conditional/comparison --- always return u a boolean
		
		System.out.println("i>j : "+ (i>j));
		System.out.println("i<j : "+ (i<j));
		System.out.println("i>=j : "+ (i>=j));
		System.out.println("i<=j : "+ (i<=j));
		System.out.println("i==j : "+ (i==j));
		System.out.println("i!=j : "+ (i!=j));
		
		
		
		// logical ops -- returns u true or false
		
		System.out.println("i>j and i!=j : "+ ( (i>j) && (i!=j) ));
		System.out.println("i<j or i!=j : "+ ( (i<j) || (i!=j) ));
		System.out.println(" NOT i>j  : "+ ( ! (i>j)));
		
		
		
		// increment n decrement ops - post pre
		
		int x=9;
		
		// ++  , post x++  ===> use , then do x+1
		// ++ , ++x ====> do x+1 , then use it
		
		System.out.println(x++);
		System.out.println(x);
		
		System.out.println(++x);
		
		
		int y=8;
		
		System.out.println(y--);
		System.out.println(y);
		System.out.println(--y);
		
		
		
		
		
	}

}
